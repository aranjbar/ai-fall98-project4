import os
import sys
import pandas as pd
import pydotplus

from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import export_graphviz
from sklearn.externals.six import StringIO
from IPython.display import Image
from random import sample
from math import sqrt


def evaluate_algorithm(dataset, features, algorithm, n_folds, *args, **kwargs):
    scores = list()
    for n in range(n_folds):
        train, test = train_test_split(dataset, test_size=0.2, random_state=13)
        y_pred = algorithm(train, test, features, *args, **kwargs)
        y_test = test.target
        accuracy = metrics.accuracy_score(y_test, y_pred)
        scores.append(accuracy)
    return scores


def build_decision_tree(x_train, y_train, *args, **kwargs):
    tree = DecisionTreeClassifier(*args, **kwargs)
    tree = tree.fit(x_train, y_train)
    return tree


def decision_tree(train, test, features, criterion='entropy', draw_tree=False, filename='decision-tree'):
    x_train = train[features]
    y_train = train.target
    x_test = test[features]
    tree = build_decision_tree(x_train, y_train, criterion=criterion)
    if draw_tree:
        dot_data = StringIO()
        export_graphviz(tree, out_file=dot_data,
                        filled=True, rounded=True,
                        special_characters=True, feature_names=features, class_names=['0', '1'])
        graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
        graph.write_png(f'../report/images/{filename}.png')
        Image(graph.create_png())
    return tree.predict(x_test)


def bagging_predict(trees, x_test):
    predictions = [tree.predict(x_test) for tree in trees]
    return [max(col, key=col.count) for col in zip(*predictions)]


def bagging(train, test, features, max_depth, sample_size, n_trees):
    trees = list()
    x_test = test[features]
    for i in range(n_trees):
        sample_train = train.sample(frac=sample_size, replace=True)
        x_train = sample_train[features]
        y_train = sample_train.target
        tree = build_decision_tree(x_train, y_train)
        trees.append(tree)
    predictions = bagging_predict(trees, x_test)
    return predictions


def random_forest_predict(trees):
    predictions = [tree.predict(x_test) for tree, x_test in trees]
    return [max(col, key=col.count) for col in zip(*predictions)]


def random_forest(train, test, features, max_depth, sample_size, n_trees):
    trees = list()
    for i in range(n_trees):
        random_feature_cols = sample(features, n_features)
        sample_train = train.sample(frac=sample_size, replace=True)
        x_train = sample_train[random_feature_cols]
        y_train = sample_train.target
        tree = build_decision_tree(x_train, y_train)
        x_test = test[random_feature_cols]
        trees.append((tree, x_test))
    predictions = random_forest_predict(trees)
    return predictions


if __name__ == "__main__":
    seed = 13
    data_path = os.path.join(os.getcwd(), 'data', 'data.csv')
    data = pd.read_csv(data_path)
    feature_cols = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg',
                    'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal']
    problem = sys.argv[1]
    # 1 - Decision Tree
    if problem == '1':
        scores = evaluate_algorithm(data, feature_cols, decision_tree, n_folds=5, criterion='entropy', draw_tree=False)
        print(f'Scores: {scores}')
        print(f'Mean Accuracy: {sum(scores) / float(len(scores))}')

    # 2 - Bagging
    if problem == '2.2':
        scores = evaluate_algorithm(data, feature_cols, bagging, n_folds=5, max_depth=6, sample_size=0.5, n_trees=5)
        print('Trees: 5')
        print(f'Scores: {scores}')
        print(f'Mean Accuracy: {sum(scores) / float(len(scores))}')

    # 2 - 3 - Remove one feature
    if problem == '2.3':
        for feature in feature_cols:
            feature_copy = list(feature_cols)
            feature_cols.remove(feature)
            scores = evaluate_algorithm(data, feature_cols, bagging, n_folds=5, max_depth=6, sample_size=0.5, n_trees=5)
            print('Trees: 5')
            print(f'Removed Features: {feature}')
            print(f'Scores: {scores}')
            print(f'Mean Accuracy: {sum(scores) / float(len(scores))}')
            print('\n')
            feature_cols = list(feature_copy)

    # 2 - 4 - Random five features
    if problem == '2.4':
        random_feature_cols = sample(feature_cols, 5)
        evaluate_algorithm(data, random_feature_cols, decision_tree, n_folds=1, criterion='entropy',
                           draw_tree=True, filename='decision-tree-5')

    if problem == '2.5':
        n_features = int(sqrt(len(feature_cols) - 1))
        scores = evaluate_algorithm(data, feature_cols, random_forest, n_folds=5, max_depth=10, sample_size=1.0, n_trees=5)
        print('Trees: 5')
        print(f'Scores: {scores}')
        print(f'Mean Accuracy: {sum(scores) / float(len(scores))}')
